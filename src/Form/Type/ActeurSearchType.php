<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Entity\Film;
use App\Repository\FilmRepository;

class ActeurSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('films', EntityType::class, array(
            'class' => Film::class,
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (FilmRepository $repo) {
                return $repo->createQueryBuilder('f')
                        ->orderBy('f.titre', 'ASC');
            },
            'label' => 'Acteurs ayant un rôle dans le(s) film(s)'
        ))
        ->add('film_min', IntegerType::class, [
            'label' => 'Nombre minimum de films',
            'required' => false
        ])
        ->add('submit', SubmitType::class, ['label' => 'Rechercher']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
