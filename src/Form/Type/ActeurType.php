<?php
namespace App\Form\Type; 

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Entity\Acteur;
use App\Entity\Film;
use App\Repository\FilmRepository;

class ActeurType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('nomPrenom', TextType::class)
                ->add('dateNaissance', DateType::class, [
                    'widget' => 'choice',
                    'years' => range(1850, date('Y')),
                ])
                ->add('nationalite', TextType::class);
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Acteur::class,
        ));
    }
}