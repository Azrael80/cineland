<?php
namespace App\Form\Type; 

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Entity\Film;
use App\Entity\Genre;
use App\Entity\Acteur;
use App\Repository\GenreRepository;
use App\Repository\ActeurRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class FilmType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('titre', TextType::class)
                ->add('genre', EntityType::class, 
                [   'class' => Genre::class,
                    'query_builder' => function (GenreRepository $repo) {
                                            return $repo->createQueryBuilder('g')
                                                    ->orderBy('g.nom', 'ASC');
                                        }
                ])
                ->add('dateSortie', DateType::class, [
                    'widget' => 'choice',
                    'years' => range(1890, date('Y')),
                ])
                ->add('duree', IntegerType::class)
                ->add('note', IntegerType::class)
                ->add('ageMinimal', IntegerType::class)
                ->add('acteurs', EntityType::class, array(
                    'class' => Acteur::class,
                    'multiple' => true,
                    'expanded' => true,
                    'query_builder' => function (ActeurRepository $repo) {
                        return $repo->createQueryBuilder('a')
                                ->orderBy('a.nomPrenom', 'ASC');
                    }
                ));
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Film::class,
        ));
    }
}