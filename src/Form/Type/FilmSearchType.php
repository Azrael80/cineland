<?php
namespace App\Form\Type; 

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Acteur;
use App\Repository\ActeurRepository;


class FilmSearchType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', TextType::class, [
                    'label' => 'Le titre contient (action 25):',
                    'required' => false
                ])
                ->add('year_1', IntegerType::class, [
                    'label' => '[Interval] Année min.',
                    'required' => false
                ])
                ->add('year_2', IntegerType::class, [
                    'label' => '[Interval] Année max.',
                    'required' => false
                ])
                ->add('enable_date_max', ChoiceType::class, [
                    'choices'  => [
                        'Non' => false,
                        'Oui' => true
                    ],
                    'data' => false,
                    'expanded' => true,
                    'label' => 'Rechercher les films antérieurs'
                ])
                ->add('date_max', DateType::class, [
                    'widget' => 'choice',
                    'years' => range(1890, date('Y')),
                    'label' => 'Date maximale'
                ])
                ->add('acteurs', EntityType::class, array(
                    'class' => Acteur::class,
                    'multiple' => true,
                    'expanded' => true,
                    'query_builder' => function (ActeurRepository $repo) {
                        return $repo->createQueryBuilder('a')
                                ->orderBy('a.nomPrenom', 'ASC');
                    },
                    'label' => 'Acteurs ayant un rôle'
                ))
                ->add('submit', SubmitType::class, ['label' => 'Rechercher']);;
    }
    
    public function configureOptions(OptionsResolver $resolver) {

    }
}