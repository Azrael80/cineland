<?php

namespace App\Entity;

use App\Repository\FilmRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @ORM\Entity(repositoryClass=FilmRepository::class)
 */
class Film
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      allowEmptyString = false,
     *      minMessage = "Le titre doit faire au minimum {{ limit }} caractères.",
     *      maxMessage = "Le titre doit faire au maximum {{ limit }} caractères."
     * )
     */
    private $titre;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive(
     *          message="La durée doit être supérieur à 0.")
     */
    private $duree;

    /**
     * @ORM\Column(type="date")
     * @Assert\LessThan("today",
     *          message="La date doit être inférieur à la date actuelle.")
     */
    private $dateSortie;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min=0,
     *      max=20,
     *      notInRangeMessage="La note doit être comprise entre {{ min }} et {{ max }}.",
     * )
     */
    private $note;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @Assert\PositiveOrZero(
     *          message="L'age minimum doit être supérieur ou égal à 0.")
     */
    private $ageMinimal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Genre", inversedBy="films", cascade="persist")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $genre;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Acteur", 
     *                      inversedBy="films", cascade="persist")
     */
    private $acteurs;

    public function __construct()
    {
        $this->acteurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getDateSortie(): ?\DateTimeInterface
    {
        return $this->dateSortie;
    }

    public function setDateSortie(\DateTimeInterface $dateSortie): self
    {
        $this->dateSortie = $dateSortie;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getAgeMinimal(): ?int
    {
        return $this->ageMinimal;
    }

    public function setAgeMinimal(int $ageMinimal): self
    {
        $this->ageMinimal = $ageMinimal;

        return $this;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /* Contraintes */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity([
            'fields' => 'titre',
            'message' => 'Un film avec ce titre existe déjà.'
        ]));
    }

    /**
     * @return Collection|Acteur[]
     */
    public function getActeurs(): Collection
    {
        return $this->acteurs;
    }

    public function addActeur(Acteur $acteur): self
    {
        if (!$this->acteurs->contains($acteur)) {
            $this->acteurs[] = $acteur;
            $acteur->addFilm($this);
        }

        return $this;
    }

    public function removeActeur(Acteur $acteur): self
    {
        if ($this->acteurs->removeElement($acteur)) {
            $acteur->removeFilm($this);
        }

        return $this;
    }

    public function __toString() {
        return $this->getTitre();
    }
}
