<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\HttpFoundation\Session\Session;
use App\Form\Type\ActeurType;
use App\Entity\Acteur;
use App\Entity\Genre;
use App\Form\Type\ActeurSearchType;

class ActeurController extends AbstractController
{
    public function index(Request $request): Response {

        $films = [];
        $film_min = 0; //Nombre de films minimum pour la recherche
        $data_title = "";
        $search = false;

        $form_search = $this->createForm(ActeurSearchType::class, 
            ['action' => $this->generateUrl('cineland_acteur_index')]);

        $form_search->handleRequest($request);

        if($form_search->isSubmitted() && $form_search->isValid()) {
            $data = $form_search->getData();
            $acteurs = [];

            //Définition du nombre de film minimum joué depuis le formulaire
            if(isset($data['film_min']) && !empty($data['film_min'])) {
                $film_min = $data['film_min'];
                $search = true; //Une recherche a été éffectuée
            }

            $acteurs = $this->getDoctrine()
                        ->getRepository(Acteur::class)
                        ->findByFilmMin($film_min);

            //Si au moins un film fait partie de la recherche, on ne prend que les acteurs ayant joué dans le ou les films demandés (Action 15)
            if(isset($data['films']) && !empty($data['films']) && $data['films']->count() > 0) {
                $films = $data['films'];
                $new_acteurs = [];

                foreach($acteurs as $a) {
                    if(count(array_intersect($films->toArray(), $a->getFilms()->toArray())) >= 1)
                        $new_acteurs[] = $a;//Si l'acteur a joué dans un des films, alors on l'ajoute aux acteurs à affiché
                }

                $acteurs = $new_acteurs;

                $search = true; //Une recherche a été éffectuée
            }

            $data_title = 'Recherche d\'acteurs (actions 15, 16)';

            //Suppression des doublons
        }
        
        if(!$search) {
            //Action 3 (Liste des acteurs)
            $data_title = 'Acteurs (action 3)';
            $acteurs = $this->getDoctrine()
                        ->getRepository(Acteur::class)->findAll();
        }

        return $this->render('acteur/index.html.twig', 
            [
                'formSearch' => $form_search->createView(),
                'data' => $acteurs,
                'data_title' => $data_title,
                'routeSee' => 'cineland_acteur_voir'
            ]);
    }

    /*
        Action 3
        Liste des acteurs
    */
    public function list(): Response
    {
        $data_title = 'Acteurs';
        $data = $this->getDoctrine()
            ->getRepository(Acteur::class)->findAll();
        return $this->render('navigation.html.twig',
            ['data_title' => $data_title,
             'data' => $data,
             'routeSee' => 'cineland_acteur_voir']);
    }

    /* 
        Action 4
        Voir un acteur
    */
    public function see(Request $request, Session $session, $id): Response {
        $repos = $this->getDoctrine()->getRepository(Acteur::class);
        $acteur = $repos->find($id);
        if(!$acteur)
            throw $this->createNotFoundException('Acteur[id='.$id.'] inexistant');

        //Formulaire pour augmenter/diminuer l'age minimal des films d'un acteur donné
        $formAge = $this->createFormBuilder(null)
            ->add('filmsAgeChange', IntegerType::class, ['label' => 'Age à ajouté ou enlevé (-):', 'required' => false, 'data' => 1])
            ->add('submit', SubmitType::class, ['label' => 'Augmenter/diminuer l\'age minimal de tous ses films'])
            ->getForm();

        $formAge->handleRequest($request);

        if ($formAge->isSubmitted() && $formAge->isValid()) {
            //Valeur à ajouté/enlevé
            $a = ($formAge->get('filmsAgeChange') !== null && !empty($formAge->get('filmsAgeChange')->getData()) ? $formAge->get('filmsAgeChange')->getData() : 1);
            foreach($acteur->getFilms() as $film) {
                //Pour chaque film on calcul le nouvel age
                $newAge = $film->getAgeMinimal() + $a;

                if($newAge >= 0) {
                    $age = $film->getAgeMinimal();
                    $film->setAgeMinimal($newAge);

                    $session->getFlashBag()
                        ->add('success', 'L\'age minimal pour le film "'.$film->getTitre().'" a été modifié '.$age.' => '.$newAge.'.');
                } else {
                    $session->getFlashBag()
                        ->add('errors', 'L\'age minimal pour le film "'.$film->getTitre().'" n\'a pas été modifié car il serait inférieur à 0.');
                }
            }

            //Les films existent déjà et sont des éléments de l'entity manager, on a juste besoin d'un flush pour
            //enregistrer les modifications
            $this->getDoctrine()->getManager()->flush(); 
        }

        return $this->render('acteur/voir.html.twig',
            [
                'acteur' => $acteur,
                'formAge' => $formAge->createView()
            ]
        );
    }

    /*
        Action 5
        Ajouter un acteur
    */
    public function add(Request $request, Session $session) {
        $acteur = new Acteur;
        $form = $this->createForm(ActeurType::class, $acteur, 
            ['action' => $this->generateUrl('cineland_acteur_ajouter')]);

        $form->add('submit', SubmitType::class, ['label' => 'Ajouter']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($acteur);
            $em->flush(); 
            $session->getFlashBag()
                ->add('success','Nouveau acteur ajouté : '.$acteur);
            return $this->redirectToRoute('cineland_acteur_voir', ['id' => $acteur->getId()]);
        }

        return $this->render('acteur/form.html.twig', 
            [
                'formTitle' => '<h1>Ajouter un acteur</h1><p><b>Action 5:</b></p>',
                'form' => $form->createView()
            ]);
    }

    /*
        Action 6
        Modifier un acteur
    */
    public function edit($id, Request $request, Session $session) {
        $repos = $this->getDoctrine()->getRepository(Acteur::class);
        $acteur = $repos->find($id);
        if(!$acteur)
            throw $this->createNotFoundException('Acteur[id='.$id.'] inexistant');

        $form = $this->createForm(ActeurType::class, $acteur, 
            ['action' => $this->generateUrl('cineland_acteur_modifier', ['id' => $acteur->getId()])]);

        $form->add('submit', SubmitType::class, ['label' => 'Modifier']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($acteur);
            $em->flush(); 
            $session->getFlashBag()
                ->add('success','Acteur modifié : '.$acteur);
            return $this->redirectToRoute('cineland_acteur_voir', ['id' => $acteur->getId()]);
        }

        return $this->render('acteur/form.html.twig', 
            [
                'formTitle' => '<h1>Modifier un acteur</h1><p><b>Action 6:</b></p>',
                'form' => $form->createView(),
                'id' => $acteur->getId()
            ]);
    }

    /*
        Action 7
        Supprimer un acteur
    */
    public function remove($id, Session $session) {
        $repos = $this->getDoctrine()->getRepository(Acteur::class);
        $acteur = $repos->find($id);

        /*
            Si l'acteur n'existe pas on ajoute une erreur,
            sinon on le supprime et on ajoute un message de succès.

            Puis on redirige sur la route d'accueil des acteurs (qui affichera le bon message).
        */

        if(!$acteur) { 
            $session->getFlashBag()
                ->add('errors','Acteur introuvable');
        } else {
            $this->getDoctrine()->getManager()->remove($acteur);
            $this->getDoctrine()->getManager()->flush();
            $session->getFlashBag()
                ->add('success','Acteur supprimé : '.$acteur);
        }
        
        return $this->redirectToRoute('cineland_acteur_index');
    }

    /* 
        Action 19
        Voir la durée des films de l'acteur
    */
    public function seeFilms($id): Response {
        $repos = $this->getDoctrine()->getRepository(Acteur::class);
        $acteur = $repos->find($id);
        if(!$acteur)
            throw $this->createNotFoundException('Acteur[id='.$id.'] inexistant');
        return $this->render('acteur/voir.films.html.twig', ['acteur' => $acteur]);
    }

    /*
        Action 20
        Voir les acteurs et leurs films dans l'ordre chronologique
        (Ajout de orderby dans la relation films de l'entité acteur)
    */
    public function listActeurFilms($searchType) {
        $data = [];

        switch($searchType) {
            case 'films':
            {
                //Action 20, récupération des acteurs et des films pour l'affichage générique de list.html.twig
                //On prend que les acteurs ayant joué dans au moins un film
                $acteurs = $this->getDoctrine()
                        ->getRepository(Acteur::class)->findByFilmMin(1);
                foreach($acteurs as $a) 
                {
                    $data[$a->getId()] = [];
                    $data[$a->getId()]['title'] = $a->getNomPrenom();
                    $list = [];

                    foreach($a->getFilms() as $f) {
                        $list[] = $f->getTitre().' ('.$f->getDateSortie()->format('d/m/Y').')';
                    }

                    $data[$a->getId()]['list'] = $list;
                };
            }
            break;
            case 'genres':
                $acteurs = $this->getDoctrine()
                        ->getRepository(Acteur::class)->findAll();
                foreach($acteurs as $a) 
                {
                    $data[$a->getId()] = [];
                    $data[$a->getId()]['title'] = $a->getNomPrenom();
                    $list = [];
                    $genres = $this->getDoctrine()
                        ->getRepository(Genre::class)->getGenreByActeurAndFilmMin($a, 1);

                    foreach($genres as $g) {
                        $list[] = $g['nom'].'('.$g['nb'].' film(s))';
                    }

                    $data[$a->getId()]['list'] = $list;
                }
                break;
            default:
                throw $this->createNotFoundException('L\'url n\'existe pas.');
                break;
        }
        return $this->render('acteur/list.html.twig', ['data' => $data]);
    }
}
