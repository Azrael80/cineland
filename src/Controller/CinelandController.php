<?php 
namespace App\Controller; 
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Genre;
use App\Entity\Film;
use App\Entity\Acteur;

class CinelandController extends AbstractController {

    public function home() {
        return $this->render('home.html.twig');
    }

    /*
        Methode permettant l'initialisation de la base de données
    */
    public function init() {
        $em = $this->getDoctrine()->getManager();

        //Ajout des genres
        $animation = new Genre;
        $animation->setNom('animation');
        $em->persist($animation);

        $policier = new Genre;
        $policier->setNom('policier');
        $em->persist($policier);

        $drame = new Genre;
        $drame->setNom('drame');
        $em->persist($drame);

        $comedie = new Genre;
        $comedie->setNom('comédie');
        $em->persist($comedie);

        $x = new Genre;
        $x->setNom('X');
        $em->persist($x);

        $romance = new Genre;
        $romance->setNom('romance');
        $em->persist($romance);

        $aventure = new Genre;
        $aventure->setNom('aventure');
        $em->persist($aventure);

        //$em->flush();

        //Ajout des acteurs
        $acteur = new Acteur;
        $acteur->setNomPrenom('Galabru Michel');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '27/10/1922'));
        $acteur->setNationalite('france');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Deneuve Catherine');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '22/10/1943'));
        $acteur->setNationalite('france');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Depardieu Gérard');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '27/12/1948'));
        $acteur->setNationalite('russie');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Lanvin Gérard');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '21/06/1950'));
        $acteur->setNationalite('france');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Désiré Dupond');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '23/12/2001'));
        $acteur->setNationalite('groland');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Tom Hanks');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '09/07/1956'));
        $acteur->setNationalite('etats-unis');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Michael Clarke Duncan');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '10/12/1957'));
        $acteur->setNationalite('etats-unis');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Omar Sy');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '20/01/1978'));
        $acteur->setNationalite('france');
        $em->persist($acteur);

        $acteur = new Acteur;
        $acteur->setNomPrenom('Louis de Funès');
        $acteur->setDateNaissance(\DateTime::createFromFormat('d/m/Y', '31/07/1914'));
        $acteur->setNationalite('france');
        $em->persist($acteur);

        $em->flush();

        //Ajout des films
        $film = new Film;
        $film->setTitre('Astérix aux jeux olympiques');
        $film->setDuree(117);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '20/01/2008'));
        $film->setNote(8);
        $film->setAgeMinimal(0);
        $film->setGenre($animation);
        $em->persist($film);

        $film = new Film;
        $film->setTitre('Le Dernier Métro');
        $film->setDuree(131);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '17/09/1980'));
        $film->setNote(15);
        $film->setAgeMinimal(12);
        $film->setGenre($drame);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Deneuve Catherine'));
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Depardieu Gérard'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('le choix des armes');
        $film->setDuree(135);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '19/10/1981'));
        $film->setNote(13);
        $film->setAgeMinimal(18);
        $film->setGenre($policier);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Deneuve Catherine'));
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Depardieu Gérard'));
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Lanvin Gérard'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('Les Parapluies de Cherbourg');
        $film->setDuree(91);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '19/02/1964'));
        $film->setNote(9);
        $film->setAgeMinimal(0);
        $film->setGenre($drame);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Deneuve Catherine'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('La Guerre des boutons');
        $film->setDuree(90);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '18/04/1962'));
        $film->setNote(7);
        $film->setAgeMinimal(0);
        $film->setGenre($comedie);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Galabru Michel'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('Forrest Gump');
        $film->setDuree(140);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '05/10/1994'));
        $film->setNote(18);
        $film->setAgeMinimal(0);
        $film->setGenre($romance);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Tom Hanks'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('La ligne verte');
        $film->setDuree(189);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '01/03/2000'));
        $film->setNote(18);
        $film->setAgeMinimal(12);
        $film->setGenre($policier);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Tom Hanks'));
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Michael Clarke Duncan'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('Intouchables');
        $film->setDuree(112);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '02/11/2011'));
        $film->setNote(17);
        $film->setAgeMinimal(0);
        $film->setGenre($drame);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Omar Sy'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('Jurrassic World');
        $film->setDuree(125);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '10/05/2015'));
        $film->setNote(15);
        $film->setAgeMinimal(0);
        $film->setGenre($aventure);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Omar Sy'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('La soupe aux choux');
        $film->setDuree(98);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '02/12/1981'));
        $film->setNote(12);
        $film->setAgeMinimal(0);
        $film->setGenre($comedie);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Louis de Funès'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('La zizanie');
        $film->setDuree(96);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '22/03/1978'));
        $film->setNote(12);
        $film->setAgeMinimal(10);
        $film->setGenre($comedie);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Louis de Funès'));
        $em->persist($film);

        $film = new Film;
        $film->setTitre('La grande vadrouille');
        $film->setDuree(132);
        $film->setDateSortie(\DateTime::createFromFormat('d/m/Y', '08/12/1966'));
        $film->setNote(17);
        $film->setAgeMinimal(10);
        $film->setGenre($comedie);
        $film->addActeur($em->getRepository(Acteur::class)->findOneByNomPrenom('Louis de Funès'));
        $em->persist($film);

        $em->flush();

        return new Response("<html><body>La base de donnée a été initialisée.</body></html>");
    }

}