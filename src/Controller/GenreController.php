<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\Range;
use App\Entity\Genre;
use App\Entity\Acteur;
use App\Repository\ActeurRepository;
use App\Form\Type\GenreType;

class GenreController extends AbstractController
{

    public function index(Request $request) {
        $genres = $acteur = $film_min = null;

        $form = $this->createFormBuilder(null)
            ->add('acteur', EntityType::class, array(
                'class' => Acteur::class,
                'query_builder' => function (ActeurRepository $repo) {
                    return $repo->createQueryBuilder('a')
                            ->orderBy('a.nomPrenom', 'ASC');
                },
                'label' => 'Acteur'
            ))
            ->add('film_min', IntegerType::class, [
                'label' => 'Nombre minimum de films joué par genre',
                'empty_data' => '1',
                'constraints' => [new Range([
                    'min' => 1,
                    'minMessage' => 'Le nombre minimum de film(s) doit être de {{ limit }} ou plus.',
                ])]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Rechercher'])
            ->getForm();

        $form->handleRequest($request);

        //Action 18: On récupère tous les genres pour les quels un acteur a joué dans un minimum de films demandé par l'utilisateur
        if($form->isSubmitted() && $form->isValid()) {
            /*
                On décide de passer par un formulaire anonyme et une requête en dehors du repository
                afin de diversifier les méthodes
            */
            $d = $form->getData();
            $acteur = $d['acteur'];
            $film_min = $d['film_min'];

            $genres = $this->getDoctrine()
                ->getRepository(Genre::class)
                ->getGenreByActeurAndFilmMin($acteur, $film_min);
        }

        return $this->render('genre/index.html.twig', [
            'formSearch' => $form->createView(),
            'genres' => $genres,
            'acteur' => $acteur,
            'film_min' => $film_min
        ]);
    }

    /*
        Action 1
        Liste des genres
    */
    public function list(): Response
    {
        $data_title = 'Genres';
        $data = $this->getDoctrine()
            ->getRepository(Genre::class)->findAll();
        return $this->render('navigation.html.twig',
            ['data_title' => $data_title,
             'data' => $data,
             'routeSee' => 'cineland_genre_voir'
            ]);
    }

    /*
        Action 2
        Ajouter un genre
    */
    public function add(Request $request, Session $session) {
        $genre = new Genre;
        $form = $this->createForm(GenreType::class, $genre, 
            ['action' => $this->generateUrl('cineland_genre_ajouter')]);

        $form->add('submit', SubmitType::class, ['label' => 'Ajouter']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($genre);
            $em->flush(); 
            $session->getFlashBag()
                ->add('success','Nouveau genre ajouté : '.$genre);
            $url = $this->generateUrl('cineland_genre_index');
            return $this->redirect($url);
        }

        return $this->render('genre/add.html.twig', 
            ['form' => $form->createView()]);
    }

    /*
        Action 22
        Afficher la durée moyenne d'un film du genre

        + Affichage de tous les films du genre
    */
    public function see($id) {
        $repos = $this->getDoctrine()->getRepository(Genre::class);
        $genre = $repos->find($id);
        if(!$genre)
            throw $this->createNotFoundException('Genre[id='.$id.'] inexistant');

        $count = $genre->getFilms()->count();
        $moy = 0;
        //Si on a au moins un film, on calcul la durée moyenne d'un film pour le genre (Action 22)
        //cette action aurait pu être effectuée avec la fonction AVG et une requête SQL
        //mais nous avons voulu utilisé la relation entre le genre et ses films
        if($count > 0) {
            foreach($genre->getFilms() as $f)
                $moy += $f->getDuree();
            $moy /= $count;
        }


        return $this->render('genre/voir.html.twig',
            [
                'genre' => $genre,
                'moyenne' => $moy
            ]
        );
    }

    /*
        Action 24
        Supprimer un genre si aucun film ne s'y rapporte
    */
    public function remove(Session $session, $id) {
        $repos = $this->getDoctrine()->getRepository(Genre::class);
        $genre = $repos->find($id);
        if(!$genre)
            throw $this->createNotFoundException('Genre[id='.$id.'] inexistant');

        if($genre->getFilms()->count() > 0) {
            //Si des films sont liés au genre, on ne le supprime pas et on prévient l'utilisateur
            $session->getFlashBag()
                ->add('errors','Le genre "'.$genre->getNom().'" ne peut pas être supprimé, un ou plusieurs film(s) s\'y rapporte(nt).');
        } else {
            //Sinon on supprime le film
            $em = $this->getDoctrine()->getManager();
            $em->remove($genre);
            $em->flush();
            $session->getFlashBag()
                ->add('success','Le genre "'.$genre->getNom().'" a été supprimé.');
        }
        return $this->redirectToRoute('cineland_genre_index');
    }
}
