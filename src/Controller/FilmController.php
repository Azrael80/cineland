<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Entity\Film;
use App\Form\Type\FilmType;
use App\Form\Type\FilmSearchType; 
use App\Entity\Acteur;
use App\Repository\FilmRepository;

/**
 * @Route("/film", name="cineland_film_")
 */
class FilmController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request): Response
    {
        $search = false;
        $films = null;
        $data = null;
        $data_title = "";
        
        $form_search = $this->createForm(FilmSearchType::class, 
            ['action' => $this->generateUrl('cineland_film_index')]);

        $form_search->handleRequest($request);

        if($form_search->isSubmitted() && $form_search->isValid()) {
            $data = $form_search->getData();
            $year_1 = $year_2 = $date_max = $acteurs = $title = null;

            //Action 13
            if(isset($data['year_1']) && !empty($data['year_1']) && isset($data['year_2']) && !empty($data['year_2'])) {
                $year_1 = $data['year_1'];
                $year_2 = $data['year_2'];
            }

            //Action 14
            if($data['enable_date_max'] && isset($data['date_max']) && !empty($data['date_max'])) {
                $date_max = $data['date_max'];
            }

            //Action 17
            if(isset($data['acteurs']) && !empty($data['acteurs'])) {
                $acteurs = $data['acteurs'];
            }

            //Action 25
            if(isset($data['title']) && !empty($data['title'])) {
                $title = $data['title'];
            }

            $data_title = 'Recherche de films (actions 13, 14, 17, 25)';
            $data = $this->getDoctrine()
                    ->getRepository(Film::class)
                    ->findBySearchForm($year_1, $year_2, $date_max, $acteurs, $title);
        } else {
            //Action 8 liste des films
            //Aucune recherche n'a été effectuée, on prend donc la liste de base
            $data_title = 'Action 8: Films';
            $data = $this->getDoctrine()
                ->getRepository(Film::class)->findAll();
        }
        
        return $this->render('film/index.html.twig', 
            [   
                'formSearch' => $form_search->createView(),
                'search' => $search,
                'data_title' => $data_title,
                'data' => $data,
                'routeSee' => 'cineland_film_voir'
            ]);
    }

    /** 
     * Action 9
     * Voir un film
     * @Route("/see/{id}", name="voir")
     */
    public function see(Request $request, Session $session, $id): Response {
        $repos = $this->getDoctrine()->getRepository(Film::class);
        $film = $repos->find($id);
        if(!$film)
            throw $this->createNotFoundException('Film[id='.$id.'] inexistant');

        //Formulaire pour augmenter la note
        $form = $this->createFormBuilder(null)
            ->add('noteDecrease', SubmitType::class, ['label' => 'Diminuer de 1'])
            ->add('noteIncrease', SubmitType::class, ['label' => 'Augmenter de 1'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Calcul de la nouvelle note (on vérifie quel bouton est appuyé)
            $newNote = $film->getNote() + ($form->get('noteDecrease')->isClicked() ? -1 : ($form->get('noteIncrease')->isClicked() ? 1 : 0));

            if($newNote >= 0 && $newNote <= 20) {
                $film->setNote($newNote);

                //Le film existe déjà et est un élément de l'entity manager, on a juste besoin d'un flush pour
                //enregistrer les modifications
                $this->getDoctrine()->getManager()->flush(); 

                $session->getFlashBag()
                    ->add('success','La note du film a été modifiée.');
            } else {
                $session->getFlashBag()
                    ->add('errors','La note du film doit ne peut être comprise qu\'entre 0 et 20.');
            }
        }

        return $this->render('film/voir.html.twig',
            [
                'film' => $film,
                'form' => $form->createView()
            ]
        );
    }

    /** 
     * Action 10
     * Ajouter un film
     * @Route("/add", name="ajouter")
     */
    public function add(Request $request, Session $session) {
        $film = new Film;
        $form = $this->createForm(FilmType::class, $film, 
            ['action' => $this->generateUrl('cineland_film_ajouter')]);

        $form->add('submit', SubmitType::class, ['label' => 'Ajouter']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush(); 
            $session->getFlashBag()
                ->add('success','Nouveau film ajouté : '.$film);
            return $this->redirectToRoute('cineland_film_voir', ['id' => $film->getId()]);
        }

        return $this->render('film/form.html.twig', 
            [
                'formTitle' => '<h1>Ajouter un film</h1><p><b>Action 10:</b></p>',
                'form' => $form->createView()
            ]);
    }

    /** 
     * Action 11
     * Modifier un film
     * @Route("/edit/{id}", name="modifier")
     */
    public function edit($id, Request $request, Session $session) {
        $repos = $this->getDoctrine()->getRepository(Film::class);
        $film = $repos->find($id);
        if(!$film)
            throw $this->createNotFoundException('Film[id='.$id.'] inexistant');

        $form = $this->createForm(FilmType::class, $film, 
            ['action' => $this->generateUrl('cineland_film_modifier', ['id' => $film->getId()])]);

        $form->add('submit', SubmitType::class, ['label' => 'Modifier']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($film);
            $em->flush(); 
            $session->getFlashBag()
                ->add('success','Film modifié : '.$film);
            return $this->redirectToRoute('cineland_film_voir', ['id' => $film->getId()]);
        }

        return $this->render('film/form.html.twig', 
            [
                'formTitle' => '<h1>Modifier un film</h1><p><b>Action 11:</b></p>',
                'form' => $form->createView(),
                'id' => $film->getId()
            ]);
    }

    /** 
     * Action 12
     * Supprimer un film
     * @Route("/rem/{id}", name="supprimer")
     */
    public function remove($id, Session $session) {
        $repos = $this->getDoctrine()->getRepository(Film::class);
        $film = $repos->find($id);

        /*
            Si le film n'existe pas on ajoute une erreur,
            sinon on le supprime et on ajoute un message de succès.

            Puis on redirige sur la route d'accueil des films (qui affichera le bon message).
        */

        if(!$film) { 
            $session->getFlashBag()
                ->add('errors','Film introuvable');
        } else {
            $this->getDoctrine()->getManager()->remove($film);
            $this->getDoctrine()->getManager()->flush();
            $session->getFlashBag()
                ->add('success','Film supprimé : '.$film);
        }
        
        return $this->redirectToRoute('cineland_film_index');
    }

    /** 
     * Action 15
     * Voir les acteurs d'un film
     * @Route("/see/actors/{id}", name="voir_acteurs")
     */
    public function seeActors($id): Response {
        $repos = $this->getDoctrine()->getRepository(Film::class);
        $film = $repos->find($id);
        if(!$film)
            throw $this->createNotFoundException('Film[id='.$id.'] inexistant');
        return $this->render('film/voir.acteurs.html.twig',
            ['film' => $film]
        );
    }
}
