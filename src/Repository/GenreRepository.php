<?php

namespace App\Repository;

use App\Entity\Genre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Genre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Genre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Genre[]    findAll()
 * @method Genre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GenreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Genre::class);
    }

    public function getGenreByActeurAndFilmMin($acteur, $film_min) {
        return $this->getEntityManager()->createQuery(
                'SELECT g.nom nom, COUNT(f.id) AS nb FROM App\Entity\Genre g
                LEFT JOIN g.films f
                LEFT JOIN f.acteurs a WHERE a = :acteur
                GROUP BY g.id
                HAVING nb >= :film_min
                ORDER BY nb DESC
                '
            )
            ->setParameter('acteur', $acteur)
            ->setParameter('film_min', $film_min)
            ->getResult();
    }
}
