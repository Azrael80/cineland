<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Film::class);
    }

    /*
        Recherche
    */
    /* Recherche à partir du formulaire
        Action 13:
            $year_1: Année minimum
            $year_2: Année maximum
        
        Action 14:
            $date_max: La date maximale

        Action 17:
            $acteurs: Liste des acteurs ayant un rôle dans le film
    */
    public function findBySearchForm($year_1, $year_2, $date_max, $acteurs, $title) {
        $q = $this->createQueryBuilder('f');

        if($year_1 != null && $year_2 != null) {
            $q->andWhere('f.dateSortie BETWEEN :d1 AND :d2')
            ->setParameter('d1', \DateTime::createFromFormat('d/m/Y H:i',"01/01/$year_1 00:00"))
            ->setParameter('d2', \DateTime::createFromFormat('d/m/Y H:i',"31/12/$year_2 23:59"));
        }

        if($date_max != null) {
            $q->andWhere('f.dateSortie < :d_max')
            ->setParameter('d_max', $date_max);
        }

        if($acteurs != null) {
            //On prend chaque acteur de la recherche, et on vérifie pour chaque film s'il contient bien l'acteur à l'aide d'une jointure
            foreach($acteurs as $a) {
                $q->join('f.acteurs', 'a_'.$a->getId(), 'WITH', 'a_'.$a->getId().'.id = '.$a->getId());
            }
        }

        //On ne prend que les films dont le titre contient $title
        if($title != null) {
            $q->andWhere("f.titre LIKE :title")
            ->setParameter('title', "%$title%");
        }

        return $q->getQuery()->getResult();
    }
}
