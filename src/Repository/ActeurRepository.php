<?php

namespace App\Repository;

use App\Entity\Acteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Acteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Acteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Acteur[]    findAll()
 * @method Acteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActeurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Acteur::class);
    }

    /* Recherche à partir d'un nombre de films minimum
        $film_min = Nombre de films minimum
    */
    public function findByFilmMin($film_min) {
        /*
            On ne récupère que les acteurs ayant eu un rôle dans $film_min films
            utilisation de leftJoin car la jointure n'est pas obligatoire, on en a besoin que pour la fonction count
            et pour une recherche avec 0 films, on doit aussi récupérer les acteurs qui n'ont pas eu de rôle.
        */
        $q = $this->createQueryBuilder('a')
            ->leftJoin('a.films', 'f')
            ->groupBy('a.id')
            ->having('COUNT(f.id) >= :film_min');

        $q->setParameter('film_min', $film_min);

        return $q->getQuery()->getResult();
    }
}
